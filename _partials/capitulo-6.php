<div class="max-layout">
    <div class="container-title-content space">
        <div
            class="container-section-title">
            <div
                class="figure-panal-section text-panal-section bg-panal-purple bg-panal-number"></div>
            <h2 class="secondary-title">La llegada de los 12 + 1</h2>
        </div>
        <div class="container-content-section container-content-section--is-reverse">
            <div class="container-video-section">
                <video
                    id="videoCap6"
                    class="video-section-path"
                    poster="_private/img/capitulos_portadas/06.png"
                    src="_private/video/cap-06.mp4"
                    type="video/mp4"></video>
                <div class="video-buttons">
                    <div class="first">
                        <button class="vid_btn" type="button" data-video="videoCap6">
                            <i class="icon-circle icon-play"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="container-description-btn">
                <p class="description">
                    Los mejores 12, el resultado del compromiso, esfuerzo, entusiasmo y la
                    experiencia dieron sus frutos.
                </p>
                <a href="#carousel-6" class="btn-secondary show-scenes"></a>
            </div>

            
        </div>
    </div>


    <input id="close-6" class="close d-none" type="checkbox">
    <label for="close-6">
        <div id="carousel-6" class="close-btn">
            <img class="arrow-up" src="_private/img/chevron-arrow-down.svg" alt="">
            <img class="arrow-down" src="_private/img/chevron-arrow-down.svg" alt="">
        </div>
    </label>

    <div class="owl-carousel owl-theme space-md">
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-6/img-1.jpg)">
                <div class="content-item-number">1</div>
            </div>
            <div class="content-item-desc">
                Para completar el gran inicio, llegaron marcas que confiaron en nosotros para
                brindar nuestros servicios de consultoría, outsourcing y conocimiento técnico.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-6/img-2.jpg)">
                <div class="content-item-number">2</div>
            </div>
            <div class="content-item-desc">
                Así como Batman necesitó a Robin, el Chavo a su barril, Superman a su capa, Thor a su martillo, Beez necesitó a un gran aliado para unir fuerzas en este maravilloso proyecto creativo. Su llegada fue el impulso que le dio el toque especial para completar el rompecabezas. Rau, un líder innato nos apoyó con su profesionalismo, buena disposición. ¡Todo un visionario!
            </div>
        </div>
    </div>
    <div class="container-deco-bee-5 bee">
        <img src="_private/img/deco-bee.svg" alt="">
    </div>
</div>