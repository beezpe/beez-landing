<div class="max-layout">
    <div class="container-title-content space">
        <div class="container-section-title ">
            <div
                class="figure-panal-section text-panal-section bg-panal-purple bg-panal-number"></div>
            <h2 class="secondary-title">Un cambio con propósito</h2>
        </div>

        <div class="container-content-section container-content-section-even">
            <div class="container-video-section">
                <div class="video-section">
                    <video
                        id="videoCap3"
                        class="video-section-path"
                        poster="_private/img/capitulos_portadas/03.png"
                        src="_private/video/cap-03.mp4"
                        type="video/mp4"></video>

                    <div class="video-buttons">
                        <div class="first">
                            <button class="vid_btn" type="button" data-video="videoCap3">
                                <i class="icon-circle icon-play"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-description-btn">
                <p class="description">
                    ¿Cómo me llamaré? La aventura para encontrar el nombre de una marca que quedará
                    grabado en la memoria de todos.
                </p>
                <a href="#carousel-3" class="btn-secondary show-scenes"></a>
            </div>
        </div>
    </div>
    <input id="close-3" class="close d-none" type="checkbox">
    <label for="close-3">
        <div id="carousel-3" class="close-btn">
            <img class="arrow-up" src="_private/img/chevron-arrow-down.svg" alt="">
            <img class="arrow-down" src="_private/img/chevron-arrow-down.svg" alt="">
        </div>
    </label>
    <div class="owl-carousel owl-theme space-md">
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-3/img-1.jpg)">
                <div class="content-item-number">1</div>
            </div>
            <div class="content-item-desc">
                Todo estaba listo en Lima para dar el gran paso. En el momento menos esperado,
                recibimos una llamada que cambió el rumbo.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-3/img-2.jpg)">
                <div class="content-item-number">2</div>
            </div>
            <div class="content-item-desc">
                Como buen emprendedor, me plantee el propósito de perfeccionar mis habilidades.
                Adquirí nuevos desafíos. Esto retrasaría la meta, pero aseguraría grandes
                frutos: La experiencia.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-3/img-3.jpg)">
                <div class="content-item-number">3</div>
            </div>
            <div class="content-item-desc">
                Pasaron meses y aún no lográbamos concretar el sueño. Sin embargo, estábamos
                alzando vuelo poco a poco al participar en los proyectos de Belcorp y AFP Prima.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-3/img-4.jpg)">
                <div class="content-item-number">4</div>
            </div>
            <div class="content-item-desc">
                Durante la travesía laboral, se originó una inspiración en el área empresarial.
                Se trataba de la participación en un proyecto digital orientado al sector Banca.
                Para nuestro asombro, días después recibimos la llamada de un banco. Era el BCP,
                quien nos invitó para una entrevista. Pero antes de ello, nos dejaron una
                importante tarea de diseño a resolver. Solo teníamos dos días para lograrlo.
                Entonces, nos atrevimos y nos pusimos manos a la obra.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-3/img-5.jpg)">
                <div class="content-item-number">5</div>
            </div>
            <div class="content-item-desc">
                Fueron las 48 horas más intensas donde nuestros mejores aliados fueron: La
                computadora, post-its, e-books, benchmarking y por supuesto, full adrenalina.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-3/img-6.jpg)">
                <div class="content-item-number">6</div>
            </div>
            <div class="content-item-desc">
                Al llegar el gran día, ocurrió algo especial. Apreciamos una brisa que nos hizo
                sentirnos cerca del lugar. Era una conexión particular. Eso doblegó esta
                sensación para ser parte del banco.
            </div>
        </div>
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-3/img-7.jpg)">
                <div class="content-item-number">7</div>
            </div>
            <div class="content-item-desc">
                A pesar del gran esfuerzo, sentí que no di lo mejor. Horas después, mientras
                pensaba en el auto sobre lo ocurrido, llegó una llamada sorprendente que me hizo
                cambiar el semblante.
            </div>
        </div>
    </div>
    <div class="container-deco-bee-4 bee">
        <img src="_private/img/deco-bee.svg" alt="">
    </div>
</div>