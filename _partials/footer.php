<footer id="site-footer" class="site-footer">
    <section class="max-layout">
        <div class="container-contact">
            <div class="container-contact-content">
                <img src="_private/img/contact.svg" alt="">
                <h2 class="secondary-title">¿Deseas contactarte con nosotros?</h2>
                <p class="description-secondary">Escribe aquí tu mensaje para poder ayudarte</p>
                <form class="contact-form">
                    <textarea id="iContact" class="contact-textarea" placeholder="Ingresa tu mensaje"></textarea>
                    <button id="bContact" class="button button--is-primary m-t-20" type="button">Enviar mensaje</button>
                </form>
            </div>
        </div>
    </section>

    <div class="copyright">
        <div class="container-social-media">
            <div class="bg-social-icon">
                <img src="_private/img/icons/icon-ws-completo.svg" alt="">
            </div>
            <div class="bg-social-icon">
                <img src="_private/img/icons/icon-messenger-completo.svg" alt="">
            </div>
            <div class="bg-social-icon">
                <img src="_private/img/icons/icon-linkedin.svg" alt="">
            </div>
            <div class="bg-social-icon">
                <img src="_private/img/icons/icon-email-completo.svg" alt="">
            </div>
        </div>
        copyright &copy; Beez.pe 2020
    </div>
</footer>

<script src="_private/vendors/Owl/jquery.min.js"></script>
<script src="_private/vendors/Owl/owl.carousel.min.js"></script>
<script src="_private/js/pages/2020.js"></script>

