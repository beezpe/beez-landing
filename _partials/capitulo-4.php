<div class="max-layout container-even">
    <div class="container-title-content space">
      
        <div class="container-section-title ">
            <div
                class="figure-panal-section text-panal-section bg-panal-purple bg-panal-number"></div>
            <h2 class="secondary-title">Cuando una puerta se cierra, otra se abre</h2>
        </div>
        <div class="container-content-section container-content-section--is-reverse">
            <div class="container-video-section">
                <video
                    id="videoCap4"
                    class="video-section-path"
                    poster="_private/img/capitulos_portadas/04.png"
                    src="_private/video/cap-04.mp4"
                    type="video/mp4"></video>
                <div class="video-buttons">
                    <div class="first">
                        <button class="vid_btn" type="button" data-video="videoCap4">
                            <i class="icon-circle icon-play"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="container-description-btn">
                <p class="description">
                    El momento que nos condujo a nuevas oportunidades, pero con un desenlace
                    desalentador.
                </p>
                <a href="#carousel-4" class="btn-secondary show-scenes"></a>
            </div>
        </div>
    </div>
    <div class="container-deco-bee-2 bee">
        <img src="_private/img/deco-bee.svg" alt="">
    </div>
    <div class="container-deco-bee-3 bee">
        <img src="_private/img/deco-bee.svg" alt="">
    </div>
    <input id="close-4" class="close d-none" type="checkbox">
    <label for="close-4">
        <div id="carousel-4" class="close-btn">
            <img class="arrow-up" src="_private/img/chevron-arrow-down.svg" alt="">
            <img class="arrow-down" src="_private/img/chevron-arrow-down.svg" alt="">
        </div>
    </label>
    <div class="owl-carousel owl-theme space-md">
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-4/img-1.jpg)">
                <div class="content-item-number">1</div>
            </div>
            <div class="content-item-desc">
                A pesar que en la vida puedan cerrarse algunas puertas, las dificultades son
                oportunidades ganadas que nos permiten ser más sabios y fuertes.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-4/img-2.jpg)">
                <div class="content-item-number">2</div>
            </div>
            <div class="content-item-desc">
                Logré una nueva oportunidad de participar en un proyecto bancario. ¿Y quién lo
                diría? Estaba casi al frente de nuestros ojos. Es aquí donde la preparación y la
                oportunidad se encuentran para producir un cambio.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-4/img-3.jpg)">
                <div class="content-item-number">3</div>
            </div>
            <div class="content-item-desc">
                Transcurrieron días y horas. Hasta que nos llamaron para darnos una excelente
                noticia: Nuestra incorporación al Proyecto Titán en el Banco Interbank. Ese
                momento fue único. Vinieron muchos escenarios a mi memoria. Todas incluían
                aquellos deseos de poder trabajar un día dentro de las instalaciones de la torre
                del banco Interbank.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-4/img-4.jpg)">
                <div class="content-item-number">4</div>
            </div>
            <div class="content-item-desc">
                Los momentos laborando en el banco fueron increíbles. Los colaboradores eran tan
                auténticos. Las cosas marchaban de maravilla, hasta que se presentó un percance
                familiar, algo impredecible y difícil que podía repercutir en el rendimiento
                profesional. Y pues sí, se hizo notorio en poco tiempo.
            </div>
        </div>
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-4/img-5.jpg)">
                <div class="content-item-number">5</div>
            </div>
            <div class="content-item-desc">
                Sucedió una situación peculiar. El jefe, al ver irregularidad en nuestro
                rendimiento, nos llamó, pero no para conversar, sino para sacarnos del proyecto
                arbitrariamente sin pedir explicaciones. Fue uno de los instantes más tristes,
                pero colmado de aprendizaje. Entendimos la importancia de definir mejor nuestra
                esencia: Empatizar con los colaboradores y más aún con los clientes
            </div>
        </div>
    </div>
</div>