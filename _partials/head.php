<meta charset="utf-8">
<meta name="encoding" charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html">
<meta name="robots" content="all, index, follow">
<meta name="googlebot" content="all, index, follow">
<meta http-equiv="cache-control" content="no-cache">
<meta property="og:type" content="website">
<meta name="twitter:card" content="summary_large_image">
<meta property="og:locale" content="es_ES">
<meta property="og:locale:alternate" content="es_ES">
<meta http-equiv="content-language" content="es_ES">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="owner" content="Beez.pe">
<meta name="author" content="Beez.pe">
<meta name="publisher" content="Beez.pe">
<meta name="copyright" content="Beez.pe">
<meta name="twitter:creator" content="Beez.pe">
<meta name="twitter:site" content="Beez.pe">
<meta name="generator" content="Beez.pe">
<meta name="organization" content="Beez.pe">
<meta property="og:image:width" content="598">
<meta property="og:image:height" content="274">
<link rel="icon" sizes="192x192" href="img/logo/favicon.png">
<link rel="icon" sizes="32x32" href="img/logo/favicon.png">
<meta property="twitter:image" content="img/logo/favicon.png">
<meta property="og:image" content="img/logo/favicon.png">
<meta property="og:image:secure_url" content="img/logo/favicon.png">
<link rel="apple-touch-icon-precomposed" href="img/logo/favicon.png">
<meta name="msapplication-TileImage" content="img/logo/favicon.png">
<link rel="icon" type="image/jpg" href="img/logo/favicon.png">
<meta property="og:url" content="https://beez.pe">
<meta property="og:site_name" content="https://beez.pe">
<meta property="twitter:url" content="https://beez.pe">
<link rel="canonical" href="https://beez.pe">
<link rel="shortlink" href="https://beez.pe">
<link rel="dns-prefetch" href="//s.w.org">
<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link
    href="https://fonts.gstatic.com"
    crossorigin="crossorigin"
    rel="preconnect">
<meta
    name="description"
    content="Laboratorio de Diseño e Ingeniería , especializados en la creación de productos/servicios con un alto nivel de innovación , tecnología y una buena dosis de empatía digital.">
<meta
    name="twitter:description"
    content="Laboratorio de Diseño e Ingeniería , especializados en la creación de productos/servicios con un alto nivel de innovación , tecnología y una buena dosis de empatía digital.">
<meta
    property="og:description"
    content="Laboratorio de Diseño e Ingeniería , especializados en la creación de productos/servicios con un alto nivel de innovación , tecnología y una buena dosis de empatía digital.">
<meta name="keywords" content="">
<meta property="og:title" content="Beez.pe - Empatía Digital">
<meta name="twitter:title" content="Beez.pe - Empatía Digital">
<meta name="dcterms.title" content="Beez.pe - Empatía Digital">
<meta name="DC.title" content="Beez.pe - Empatía Digital">
<meta name="application-name" content="Titulo de la página">
<meta name="title" content="Beez.pe - Empatía Digital">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link
    href="https://fonts.gstatic.com"
    crossorigin="crossorigin"
    rel="preconnect">
<meta
    name="description"
    content="Laboratorio de Diseño e Ingeniería , especializados en la creación de productos/servicios con un alto nivel de innovación , tecnología y una buena dosis de empatía digital.">
<meta
    name="twitter:description"
    content="Laboratorio de Diseño e Ingeniería , especializados en la creación de productos/servicios con un alto nivel de innovación , tecnología y una buena dosis de empatía digital.">
<meta
    property="og:description"
    content="Laboratorio de Diseño e Ingeniería , especializados en la creación de productos/servicios con un alto nivel de innovación , tecnología y una buena dosis de empatía digital.">
<meta name="keywords" content="">
<meta property="og:title" content="Beez.pe - Empatía Digital">
<meta name="twitter:title" content="Beez.pe - Empatía Digital">
<meta name="dcterms.title" content="Beez.pe - Empatía Digital">
<meta name="DC.title" content="Beez.pe - Empatía Digital">
<meta name="application-name" content="Titulo de la página">
<meta name="title" content="Beez.pe - Empatía Digital">
<title>BEEZ 💡 EMPATÍA DIGITAL 👋  </title>
