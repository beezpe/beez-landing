<div class="max-layout container-even">
    <div class="container-title-content space">
        <div class="container-section-title ">
            <div
                class="figure-panal-section text-panal-section bg-panal-purple bg-panal-number"></div>
            <h2 class="secondary-title">Con la mirada en nuevos horizontes</h2>
        </div>
        <div class="container-content-section">
            <div class="container-video-section">
                <video
                    id="videoCap7"
                    class="video-section-path"
                    poster="_private/img/capitulos_portadas/07.png"
                    src="_private/video/cap-07.mp4"
                    type="video/mp4"></video>
                <div class="video-buttons">
                    <div class="first">
                        <button class="vid_btn" type="button" data-video="videoCap7">
                            <i class="icon-circle icon-play"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="container-description-btn">
                <p class="description">
                    El secreto mejor guardado para cumplir grandes objetivos en este 2021. ¿Quieres
                    saber de qué se trata?
                </p>
                <a href="#carousel-7" class="btn-secondary show-scenes"></a>
            </div>
        </div>
    </div>
    <input id="close-7" class="close d-none" type="checkbox">
    <label for="close-7">
        <div id="carousel-7" class="close-btn">
            <img class="arrow-up" src="_private/img/chevron-arrow-down.svg" alt="">
            <img class="arrow-down" src="_private/img/chevron-arrow-down.svg" alt="">
        </div>
    </label>
    <div class="owl-carousel owl-theme space-md">
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-7/img-1.jpg)">
                <div class="content-item-number">1</div>
            </div>
            <div class="content-item-desc">
                El 2020 fue excelente, lleno de éxito y productividad. Sin embargo, nos
                olvidamos de algo importante: Nuestra web interactiva. Es por ello que pensamos,
                definimos y ejecutamos. ¡Manos a la obra con la web!
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-7/img-2.jpg)">
                <div class="content-item-number">2</div>
            </div>
            <div class="content-item-desc">
                La web que me representa, transmitirá la esencia de mis orígenes y mi verdadera
                identidad en el mundo digital.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-7/img-3.jpg)">
                <div class="content-item-number">3</div>
            </div>
            <div class="content-item-desc">
                Me haré presente de manera progresiva en las redes sociales y profesionales,
                para dar a conocer un poco más de mí y sobre todo lo que puedo hacer por ti.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-7/img-4.jpg)">
                <div class="content-item-number">4</div>
            </div>
            <div class="content-item-desc">
                Este 2021 nos tiene preparado grandes cosas. Espera que muy pronto saldrán
                nuevos capítulos en nuestro Episodio 2. Emprenderemos nuevos retos. Seguiremos
                trabajando con entusiasmo, compromiso y creatividad para satisfacer las
                necesidades de nuestros clientes, porque más que una marca, somos una gran
                familia.
            </div>
        </div>
    </div>
    <div class="container-deco-bee-4 bee">
        <img src="_private/img/deco-bee.svg" alt="">
    </div>
</div>