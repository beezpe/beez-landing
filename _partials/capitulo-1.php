<div class="max-layout">
    <div class="container-title-content space">
        <div class="container-section-title ">
            <div
                class="figure-panal-section text-panal-section bg-panal-purple bg-panal-number"></div>
            <h2 class="secondary-title">El principio de la historia</h2>
        </div>
        <div class="container-content-section">
            <div class="container-video-section">
                <div class="video-section">
                    <video
                        id="videoCap1"
                        class="video-section-path"
                        poster="_private/img/capitulos_portadas/01.png"
                        src="_private/video/cap-01.mp4"
                        type="video/mp4"></video>

                    <div class="video-buttons">
                        <div class="first">
                            <button class="vid_btn" type="button" data-video="videoCap1">
                                <i class="icon-circle icon-play"></i>
                            </button>
                        </div>
                    </div>

                </div>

            </div>
            <div class="container-description-btn">
                

                <p class="description">
                    El comienzo de una gran historia. La decisión y determinación para dar a conocer
                    una esencia única en el mundo digital. Todo estaba listo para empezar algo
                    grandioso

                </p>
                <a href="#carousel-1" class="btn-secondary show-scenes"></a>
            </div>
        </div>
    </div>
    <input id="close-1" class="close d-none" type="checkbox">
    <label for="close-1">
        <div id="carousel-1" class="close-btn">
            <img class="arrow-up" src="_private/img/chevron-arrow-down.svg" alt="">
            <img class="arrow-down" src="_private/img/chevron-arrow-down.svg" alt="">
            <p class="content-escenas">Escenas del capítulo</p>
        </div>
    </label>
    <div class="owl-carousel owl-theme space-md">
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-1/img-1.jpg)">
                <div class="content-item-number">1</div>
            </div>
            <div class="content-item-desc">
                Érase una vez en diciembre de 2018, se estaba culminando la implementación de un
                proyecto denominado SIJE. Se trataba de una estrategia de transformación digital
                muy importante para una entidad pública, denominada JNE.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-1/img-2.jpg)">
                <div class="content-item-number">2</div>
            </div>
            <div class="content-item-desc">
                En 2019, justamente por el camino de la costa peruana, se iba dando forma de las
                características de mi existencia.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-1/img-3.jpg)">
                <div class="content-item-number">3</div>
            </div>
            <div class="content-item-desc">
                Estaba decidido que mi esencia era la de ser parte de la creación de soluciones
                digitales que escucharan las necesidades de los usuarios y se complementen con
                los objetivos de cada cliente.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-1/img-4.jpg)">
                <div class="content-item-number">4</div>
            </div>
            <div class="content-item-desc">
                ¡Todo estaba listo para empezar algo grandioso!
            </div>
        </div>

    </div>

</div>
