<div class="max-layout container-even">
    <div class="container-title-content space">
        <div class="container-section-title ">
            <div
                class="figure-panal-section text-panal-section bg-panal-purple bg-panal-number"></div>
            <h2 class="secondary-title">Un nombre,la esencia de todo</h2>
        </div>
        <div class="container-content-section container-content-section--is-reverse">

            <div class="container-video-section">
                <div class="video-section">
                    <video
                        id="videoCap2"
                        class="video-section-path"
                        poster="_private/img/capitulos_portadas/02.png"
                        src="_private/video/cap-02.mp4"
                        type="video/mp4"></video>

                    <div class="video-buttons">
                        <div class="first">
                            <button class="vid_btn" type="button" data-video="videoCap2">
                                <i class="icon-circle icon-play"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-description-btn">
                <p class="description">
                    ¿Cómo me llamaré? La aventura para encontrar el nombre de una marca que quedará
                    grabado en la memoria de todos.
                </p>
                <a href="#carousel-2" class="btn-secondary show-scenes"></a>
            </div>
        </div>
    </div>
    <div class="container-deco-bee-2 bee">
        <img src="_private/img/deco-bee.svg" alt="">
    </div>
    <input id="close-2" class="close d-none" type="checkbox">
    <label for="close-2">
        <div id="carousel-2" class="close-btn">
            <img class="arrow-up" src="_private/img/chevron-arrow-down.svg" alt="">
            <img class="arrow-down" src="_private/img/chevron-arrow-down.svg" alt="">
        </div>
    </label>
    <div class="owl-carousel owl-theme space-md">
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-2/img-1.jpg)">
                <div class="content-item-number">1</div>
            </div>
            <div class="content-item-desc">
                ¿Cómo me llamaré? Fue una de las preguntas que rondó una y otra vez en mi
                cabeza. Fue un verdadero reto. Quería transmitir un concepto original y ameno
                que quedará grabado en la memoria de todos.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-2/img-2.jpg)">
                <div class="content-item-number">2</div>
            </div>
            <div class="content-item-desc">
                Confieso que fueron muchas pruebas para hallar el sello ideal. Hubo varias
                propuestas: Abejota, Abeja Digital, Digital Bee, ABJ Digital y Abejorro.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-2/img-3.jpg)">
                <div class="content-item-number">3</div>
            </div>
            <div class="content-item-desc">
                Después de un benchmark, una serie de iteraciones y encuestas, decidieron darme
                el gran nombre de Beez, el cual tiene un concepto familiar y cristiano.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-2/img-4.jpg)">
                <div class="content-item-number">4</div>
            </div>
            <div class="content-item-desc">
                Las letras A,B y J, en su conjunto se escuchan como Abeja, son las iniciales de
                3 personas especiales(Ariana, Benjamin, Joab). Esta palabra en inglés se traduce
                como Bee, y por la cual al querer incorporar el número 7, un número simbólico en
                la biblia, se creó una armonía entre ambos y nació el nombre de Beez.
            </div>
        </div>
    </div>
    <div class="container-deco-bee-3 bee">
        <img src="_private/img/deco-bee.svg" alt="">
    </div>
</div>
