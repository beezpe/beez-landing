<div class="max-layout container-even">
    <div class="container-title-content space">
        <div
            class="container-section-title">
            <div
                class="figure-panal-section text-panal-section bg-panal-purple bg-panal-number"></div>
            <h2 class="secondary-title">2020, el año de la transformación</h2>
        </div>
        <div class="container-content-section">
            <div class="container-video-section">
                <video
                    id="videoCap5"
                    class="video-section-path"
                    poster="_private/img/capitulos_portadas/05.png"
                    src="_private/video/cap-05.mp4"
                    type="video/mp4"></video>
                
                <div class="video-buttons">
                    <div class="first">
                        <button class="vid_btn" type="button" data-video="videoCap5">
                            <i class="icon-circle icon-play"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="container-description-btn auto-margin">
                <p class="description">
                    Un viaje, un nuevo camino, más desafíos y metas extraordinarias que nos hicieron
                    evolucionar.
                </p>
                <a href="#carousel-5" class="btn-secondary show-scenes"></a>
            </div>
        </div>
    </div>
    <input id="close-5" class="close d-none" type="checkbox">
    <label for="close-5">
        <div id="carousel-5" class="close-btn">
            <img class="arrow-up" src="_private/img/chevron-arrow-down.svg" alt="">
            <img class="arrow-down" src="_private/img/chevron-arrow-down.svg" alt="">
        </div>
    </label>
    <div class="owl-carousel owl-theme space-md">
        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-5/img-1.jpg)">
                <div class="content-item-number">1</div>
            </div>
            <div class="content-item-desc">
                Iniciamos el 2020 con una gran experiencia. Acampamos en Kawai, una playa
                hermosa para poder reflexionar y trazar planes.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-5/img-2.jpg)">
                <div class="content-item-number">2</div>
            </div>
            <div class="content-item-desc">
                Al ritmo del sonido del mar y disfrutando las caricias de la brisa del mar,
                mientras el ocaso del sol se proyectaba, la decisión de empezar Beez estaba
                decidido, y ya teníamos el slogan: Empatía Digital
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-5/img-3.jpg)">
                <div class="content-item-number">3</div>
            </div>
            <div class="content-item-desc">
                Al regresar a casa, nos contactaron de dos instituciones para compartir
                conocimientos. Esto despertó un sueño que faltaba cumplir: Ser docente y así
                poder formar nuevas generaciones.
            </div>
        </div>

        <div class="item content-item">
            <div
                class="content-item-img"
                style="background-image: url(_private/img/carousel/capitulo-5/img-4.jpg)">
                <div class="content-item-number">4</div>
            </div>
            <div class="content-item-desc">
                Ser docente de IDAT y que nos escogieran para enseñar en CERTUS, fue uno de los
                objetivos más importantes dentro de nuestro desarrollo profesional.
            </div>
        </div>
    </div>
    <div class="container-deco-bee-3 bee">
        <img src="_private/img/deco-bee.svg" alt="">
    </div>
</div>