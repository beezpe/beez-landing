<!DOCTYPE html>
<html lang="es">

    <head>
        
        <?php include '_partials/head.php' ?>
        
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="_private/css/pages/core.css">
        <link rel="stylesheet" href="_private/css/site.css">
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.css">
    </head>

    <body id="site" class="site">
        <header id="site-header" class="site-header cleaner">
            <div class="menu-fixed">
                <div class="logo-fixed">
                    <img src="_private/img/Logo-Beez-1.svg">
                </div>
                <nav class="nav-title">
                    <h2 class="title-fixed">Un vuelo más dulce que la miel</h2>
                    <div class="vectores-de-anclaje vectores-de-anclaje-fixed">
                        <a class="menu-link" href="#capitulo-1">
                            <div class="bg-panal-yellow bg-panal-yellow-1"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-2">
                            <div class="bg-panal-yellow bg-panal-yellow-2"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-3">
                            <div class="bg-panal-yellow bg-panal-yellow-3"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-4">
                            <div class="bg-panal-yellow bg-panal-yellow-4"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-5">
                            <div class="bg-panal-yellow bg-panal-yellow-5"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-6">
                            <div class="bg-panal-yellow bg-panal-yellow-6"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-7">
                            <div class="bg-panal-yellow bg-panal-yellow-7"></div>
                        </a>
                    </div>
                </nav>
            </div>
            <div class="logo">
                <img src="_private/img/Logo-Beez-1.svg">
            </div>
            <div class="redes-sociales"></div>
            <div class="main-container-header">
                <div class="container-dos-mil-veinte">
                    <h1 class="main-title">Un vuelo más dulce que la miel</h1>
                    <div class="vectores-de-anclaje">
                        <a class="menu-link" href="#capitulo-1">
                            <div class="bg-panal-yellow bg-panal-yellow-1"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-2">
                            <div class="bg-panal-yellow bg-panal-yellow-2"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-3">
                            <div class="bg-panal-yellow bg-panal-yellow-3"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-4">
                            <div class="bg-panal-yellow bg-panal-yellow-4"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-5">
                            <div class="bg-panal-yellow bg-panal-yellow-5"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-6">
                            <div class="bg-panal-yellow bg-panal-yellow-6"></div>
                        </a>
                        <a class="menu-link" href="#capitulo-7">
                            <div class="bg-panal-yellow bg-panal-yellow-7"></div>
                        </a>
                    </div>
                    <p class="description-2020">

                        ¡Hola! soy <strong class="beez-name">Beez</strong> ¿Te gustaría conocer un poco de mi historia? Comencemos. Para alcanzar el éxito, debemos recorrer un largo camino. Además, se necesita fuerza de voluntad, determinación y por supuesto, hacer todo con amor y pasión. Todo inició con una maleta cargada de sueños, miedos, pero con mucho entusiasmo. Te contaré en breves etapas cómo inició esta travesía, cada acierto, uno que otro desafío y lo gratificante que fue llegar a la meta. Ponte cómodo y presta atención, porque en este viaje, serás mi invitado especial. ¿Estás listo?

                    </p>
                    <div class="main-button-content">
                        <a class="menu-link button button--is-primary m-t-20" href="#capitulo-1">Listo para empezar</a>
                    </div>

                    

                </div>
                <div class="container-main-img">
                    <img class="main-img" src="_private/img/main-img.svg" alt="">
                </div>
                
            </div>
            <div class="container-deco-mouse">
                <img src="_private/img/deco-mouse.svg" alt="">
                <span class="text-deco-mouse">NAVEGA HACIA ABAJO</span>
                <img src="_private/img/line-deco-mouse.svg" alt="">
            </div>
        </header>
        
        
        


        <main id="site-main" class="site-main">
            

            <section id="capitulo-1" class="ancla start">
                
                <?php include "_partials/decoration.html" ?>
                <?php include "_partials/capitulo-1.php" ?>
            </section>

            <section id="capitulo-2" class="ancla beez-even">
                <?php include "_partials/decoration.html" ?>
                <?php include "_partials/capitulo-2.php" ?>
            </section>

            <section id="capitulo-3" class="ancla">
                <?php include "_partials/decoration.html" ?>
                <?php include "_partials/capitulo-3.php" ?>
            </section>

            <section id="capitulo-4" class="ancla beez-even">
                <?php include "_partials/decoration.html" ?>
                <?php include "_partials/capitulo-4.php" ?>
            </section>
        
            <section id="capitulo-5" class="ancla">
                <?php include "_partials/decoration.html" ?>
                <?php include "_partials/capitulo-5.php" ?>
            </section>

            <section id="capitulo-6" class="ancla beez-even">
                <?php include "_partials/decoration.html" ?>
                <?php include "_partials/capitulo-6.php" ?>
            </section>

            <section id="capitulo-7" class="ancla">
                <?php include "_partials/decoration.html" ?>
                <?php include "_partials/capitulo-7.php" ?>
            </section>
            
        </main>

                
        <?php include '_partials/footer.php' ?>
    </body>
</html>