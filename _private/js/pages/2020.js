"use strict"

const Beez = (function () {
    const data = {
        animation_nav : document.querySelector('.menu-fixed'),
        animation_start : document.querySelector('.start'),

        vidClip1 : document.getElementById("videoCap1"),
        vidClip2 : document.getElementById("videoCap2"),
        vidClip3 : document.getElementById("videoCap3"),
        vidClip4 : document.getElementById("videoCap4"),
        vidClip5 : document.getElementById("videoCap5"),
        vidClip6 : document.getElementById("videoCap6"),
        vidClip7 : document.getElementById("videoCap7"),

        iconPlay  : "icon-play",
        iconPause : "icon-pause"
    };

    const events = function () {
        window.addEventListener('scroll', methods.menuFixed);


        

        $("#bContact").on("click", function(){
            let message = $("#iContact").val();
            methods.sendMessage(message);
        })

        $(".show-scenes").click(function (event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(this.hash).offset().top 
            }, 300, "linear");
        });

        $(".menu-link").click(function (event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(this.hash).offset().top - 100
            }, 200, "linear");
        });
     

      

        $('.vid_btn').on('click', function(event){
            let video = $(this).data('video');
            

            if($(this).children().hasClass(data.iconPlay)){
                $(this).addClass("pausa")
                document.getElementById(video).play()
                $(this).children().removeClass(data.iconPlay).addClass(data.iconPause)
            }else{
                $(this).removeClass("pausa")
                document.getElementById(video).pause()
                $(this).children().removeClass(data.iconPause).addClass(data.iconPlay)
            }

            document.getElementById(video).onended = function() {
                $(event.target).parent().removeClass("pausa")
                document.getElementById(video).currentTime = 0;
                document.getElementById(video).load();
                $(event.target).removeClass(data.iconPause).addClass(data.iconPlay)
            }
            
        })

        

    };

    const methods = {
        sendMessage : function (message) {
            let myNumber = "51959273096"
            let myMessage = message.split(' ').join('%20')
            let myURL = 'https://api.whatsapp.com/send?phone=' + myNumber + '&text=%20' + myMessage;
            window.open(myURL, '_blank');
        },
        menuFixed : function(){
            let scrollTop = document.documentElement.scrollTop;

            let height_animation = data.animation_start.offsetTop;
            if (height_animation < scrollTop) {
                data.animation_nav.style.opacity = 1;
            } else {
                data.animation_nav.style.opacity = 0;
            }
        },
        loadCarousel : function(){
            $('.owl-carousel').owlCarousel({
                loop: false,
                nav: true,
                items: 2,
                drag: false,
                mouseDrag: false,
                pullDrag: false,
                touchDrag: false,
                responsiveClass: true,
                autoHeight:true,
                responsive: {
                    0: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    1000: {
                        items: 2
                    }
                }
            });
        }
    };

    const initialize = function () {
        methods.loadCarousel()
        events();
    };

    return {init: initialize};
})();

document.addEventListener('DOMContentLoaded', function () {
    Beez.init();

}, false);